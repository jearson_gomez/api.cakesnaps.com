<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCakeThemeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cake_theme', function (Blueprint $table) {
            $table->uuid('cake_id');
            $table->integer('theme_id')->unsigned();

            $table->foreign('cake_id')
                  ->references('id')
                  ->on('cakes')
                  ->onDelete('cascade');

            $table->foreign('theme_id')
                  ->references('id')
                  ->on('themes')
                  ->onDelete('cascade');  

            $table->primary(['cake_id', 'theme_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cake_theme');
    }
}
