<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->uuid('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->boolean('active')->default(1);
            $table->string('gender')->nullable();
            $table->integer('timezone')->nullable();
            $table->string('location')->nullable();
            $table->string('password');
            $table->string('picture_url')->nullable();
            $table->string('thumbnail')->nullable();
            $table->text('bio')->nullable();
            $table->date('birthdate')->nullable();
            $table->rememberToken();
            $table->timestamps();

            $table->primary('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
