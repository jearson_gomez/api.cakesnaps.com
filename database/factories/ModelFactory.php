<?php

use App\User;
use Carbon\Carbon;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'id' => $faker->unique()->numberBetween(1000, 2000),
        'name' => $faker->unique()->name,
        'email' => $faker->unique()->safeEmail,
        'active' => rand(0,1),
        'gender' => rand(0,1) ? 'male' : 'female',
        'picture_url' => $faker->imageUrl(640, 480),
        'timezone' => mt_rand(-10, 10),
        'birthdate' => rand(0, 1) ? $faker->dateTimeBetween('-40 years', '18 years') : null,
        'password' => $password ?: $password = bcrypt('password'),
        'location' => rand(0,1) ? "{$faker->city}, {$faker->state}" : null,
        'bio' => $faker->sentence(100),
        'remember_token' => str_random(10),
    ];
});
$factory->define(App\Cake::class, function(Faker\Generator $faker){
    $name = $faker->sentence(2, true);
    return [
        'id' => $faker->unique()->numberBetween(2001, 3000),
        'name' => $name,
        'slug' => $name,
        'description' => $faker->paragraph(3, true),
        'picture_url' => $faker->imageUrl(720, 500),
        'thumbnail' => $faker->imageUrl(100, 78),
        'user_id' => factory(App\User::class)->create()->id,
        'created_at' => Carbon::now()
    ];
});
$factory->define(App\Category::class, function(Faker\Generator $faker){
    return [
        'name' => $faker->word,
        'description' => $faker->paragraph(3, true),
        'picture_url' => $faker->imageUrl(640, 480)
    ];
});
$factory->define(App\Theme::class, function(Faker\Generator $faker){

    $date = Carbon::now();

    return [
        'name' => $faker->sentence(2, false),
        'description' => $faker->paragraph(3, true),
        'picture_url' => $faker->imageUrl(640, 480),
        'end_of_competition' => $date->addDays(rand(2,15))
    ];
});

$factory->define(App\Comment::class, function(Faker\Generator $faker){
    return [
        'name' => $faker->sentence(6, true)
    ];
});



