<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       // $this->call(CakesTableSeeder::class);
        $this->call(CategoriesTableSeeder::class);
        $this->call(ThemesTableSeeder::class);
        $this->call(Oauth2Seeder::class);
    }
}
