<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$user1 = factory(App\User::class)->create([
            'email' => 'cakesnaps@example.com'
        ])->id;
        $user2 = factory(App\User::class)->create([
            'email' => 'john@example.com'
        ])->id;

        factory(App\Category::class,3)
        	->create()
        	->each(function ($category) use ($user1){
        	
        		$category->cakes()->saveMany(factory(App\Cake::class,10)->create([

        			'user_id' => $user1
        		]));
        	});

        factory(App\Category::class,2)
        	->create()
        	->each(function ($category) use ($user2){
        		$category->cakes()->saveMany(factory(App\Cake::class,20)->create([
        			'user_id' => $user2
        		]));
        	});



    }
}
