<?php

use Illuminate\Database\Seeder;

class Oauth2Seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('oauth_clients')->insert([
        	'secret' => 'secret',
        	'name' => 'android-client',
        	'redirect' => env('APP_URL') .'/auth/callback',
        	'personal_access_client' => 0,
        	'password_client' => 1,
        	'revoked' => 0
        ]);
    }
}
