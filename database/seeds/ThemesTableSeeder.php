<?php

use Illuminate\Database\Seeder;

class ThemesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$user3 = factory(App\User::class)->create([
            'email' => 'cakefight@example.com'
        ])->id;

        factory(App\Theme::class, 20)->create([

        	'user_id' => $user3
        ]);
    }
}
