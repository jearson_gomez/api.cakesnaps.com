<?php

use Illuminate\Database\Seeder;

class CakesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user1 = factory(App\User::class)->create([
            'email' => 'cakesnaps@example.com'
        ])->id;
        $user2 = factory(App\User::class)->create([
            'email' => 'john@example.com'
        ])->id;

        factory(App\Cake::class, 20)
            ->create([
                'user_id' => $user1
            ]);
        factory(App\Cake::class, 20)
            ->create([
                'user_id' => $user2
            ]);


    }
}
