## Instructions

Run 'composer install' to update dependency. 

Run 'php artisan migrate --seed' to create tables and fake data which also includes: 

	generate static keys (secret and token validation type (password grant)
	Note: generating random keys is not yet implemented
	
You may use the default user
	email: cakesnaps@example.com 
	password: password
	
	email: john@example.com	
	password: password
	
API keys used in Amazon s3, Mailtrap and Pusher is on the .env.example

	Note: uploading of images is on queue.
	Make sure to change QUEUE_DRIVER to database in your .env . e.g QUEUE_DRIVER=database
	
Run 'php artisan queue:work' to process onqueu jobs being save on the loca database.
	Note: If there are no tasks being process (depends on time)
	the queue worker will stop. It is advisable to use monitor system such as Supervisor -
	https://laravel.com/docs/5.4/queues#supervisor-configuration .


## About Cakesnaps

Cakesnaps is built for anyone who bakes or appreciates good looking food. Share snaps of your latest baking creations, ideas or experiments and receive feedback.



