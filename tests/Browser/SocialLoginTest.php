<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Chrome;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Laravel\Socialite\Facades\Socialite;

class SocialLoginTest extends DuskTestCase
{
    use DatabaseMigrations;

    /** @test */
    public function it_calls_social_login_page()
    {

        $this->browse(function ($browser) {
            $browser->visit('/login/facebook/')
                ->assertSee('Log into Facebook');
        });
    }

}
