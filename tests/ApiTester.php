<?php

namespace Tests;

use App\Cake;
use App\User;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Http\UploadedFile;

class ApiTester extends TestCase
{
    use DatabaseMigrations;
	// use WithoutMiddleware;

	/**
     * Will get file form storage
     * 
     * @return string
     */
    public function getFileFromStorage($file)
    {
        return storage_path('tests/'.$file);
    }
    /**
     * Will instantiate uploaded file class and convert image type
     * 
     * @param  string $path 
     * @return Illuminat\Http\UploadedFile
     */
    public function prepareFile($file)
    {
        $path = $this->getFileFromStorage($file);
 
        return new UploadedFile(
            $path, 
            $file, 
            'image/png', 
            filesize($path), 
            null, 
            true
        );
    }

    /**
     * Uploads Image of user
     * @param  Uploader $file 
     * @param  User   $user 
     * @return Response
     */
    public function uploadUserAvatar($file, User $user)
    {
        return $this->call('PATCH', '/users/' .$user->id .'/image', [
                'name' => $user->name,
                'email' => $user->email,
                'password' => $user->password
            ],[
                'Accept' => 'application/json'
            ],[
            'image' => $file
        ]);
    }
}