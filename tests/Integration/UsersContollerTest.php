<?php
namespace Tests\Integration;

use App\Cake;
use App\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\Auth;
use Laravel\Passport\Passport;
use Tests\ApiTester;

class UsersControllerTest extends ApiTester
{
	//use DatabaseTransactions;

	/** @test */
	public function it_should_return_list_of_users()
	{
		for($i = 0; $i < 20 ; $i++){
			factory(Cake::class, 2)->create();
		}
		$response = $this->get('/users');
		$data = json_decode($response->getContent(), true)['data'];

		$response->assertStatus(200);
		$this->assertCount(20, $data);
	}
	/** @test */
	public function it_uploads_user_image()
	{
		$user = factory(User::class)->create();
		Passport::actingAs($user);

		$file = $this->prepareFile('profile_pic.jpg');
		$response = $this->uploadUserAvatar($file, $user);
	
		$data = json_decode($response->getContent(), true);
		
		$response
		    ->assertStatus(201)
		    ->assertJson($data);
	}
	/** @test */
	public function it_updates_user_fields()
	{
		$user = factory(User::class)->create();
		Passport::actingAs($user);

		$response = $this->put('/users/' .$user->id, [
			'name' => $user->name,
			'password' => $user->password,
			'password_confirmation' => $user->password,
			'email' => $user->email
		],[
			'Accept' => 'application/json'
		]);
		$data = json_decode($response->getContent(), true);
		$response
		   ->assertStatus(201)
		   ->assertJson($data);

	}
}