<?php

namespace Tests\Integration;


use App\Cake;
use App\ImageUploader;
use Illuminate\Support\Facades\Storage;
use Tests\ApiTester;

class ImageUploaderTest extends ApiTester
{
	/**
	 * 
	 * @var uploader
	 */
	protected $uploader; 

	/**
	 * Instantiate ImageUploaderTest
	 */
	public function __construct()
	{
		$this->uploader = new ImageUploader;
	}
	/** @test */
	public function it_uploads_image_to_s3()
	{
		$cake = factory(Cake::class)->create();
		$imageId = $this->convertImageToStreamedData($cake);
		
		$response = $this->upload($cake, $imageId);

		$this->assertEquals(201, 
			$response->getStatusCode()
		);
	}
	/** @test */
	public function it_removes_streamed_image_from_storage()
	{
		$cake = factory(Cake::class)->create();
		$imageId = $this->convertImageToStreamedData($cake);

		$this->upload($cake, $imageId);

		$file = Storage::exists($imageId);
		$this->assertFalse($file);
	}

	/**
	 * Will convert image to streamed data
	 * 
	 * @param  Cake $cake 
	 * @return string
	 */
	public function convertImageToStreamedData($cake)
	{
		$file = $this->prepareFile('cakesnaps-sign-off.png');

		return $this->uploader->makePhoto($cake, $file);
	}

	/**
	 * Will upload image to s3 Amazon
	 * 
	 * @param  Cake   $cake    
	 * @param  string $imageId 
	 * @return Response
	 */
	public function upload(Cake $cake, $imageId)
	{
		$this->uploader->thumbnail->make(
			$cake->pictureBaseDir() .$imageId
		);
		return $this->uploader->saveImage(
			$cake, $imageId
		);
	}
}