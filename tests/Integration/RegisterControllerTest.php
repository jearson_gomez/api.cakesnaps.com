<?php

namespace Tests\Integration;

use App\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\ApiTester;


class RegisterControllerTest extends ApiTester
{
	use DatabaseTransactions;
	/** @test */
	public function it_registers_new_user()
	{
		$user = factory(User::class)->make();

		$response = $this->post('/register',[
			'name' => $user->name,
			'password' => $user->password,
			'password_confirmation' => $user->password,
			'email' => $user->email,
			'active' => $user->active,
			'gender' => $user->gender,
			'timezone' => $user->timezone,
			'location' => $user->location,
			'avatar'  => $user->avatar,
			'bio' => $user->bio,
			'birthdate' => $user->birthdate
		]);
		
		$response
		    ->assertStatus(201)
		    ->assertJsonStructure(['id','name','email'], 
		    	json_decode($response->getContent(),true)['data']
		    );

	}

	/** @test */
	public function it_validates_required_fields()
	{
		$user = factory(User::class)->make();
		$response = $this->post('/register', [

		],[
			'accept' => 'application/json'
		]);
		$data = json_decode($response->getContent(), true);
		$response
			->assertJson($data)
			->assertStatus(422);
	}
}