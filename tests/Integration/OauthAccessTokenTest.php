<?php
namespace App\Tests\Integration;

use App\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Support\Facades\Artisan;
use Tests\TestCase;


class OauthAccessTokenTest extends TestCase
{
	/** @test */
	public function it_test_something()
	{
		$this->assertTrue(true);
	}
	/**
	 * Uncomment test below when testing token.
	 * When running this test, run this class alone to avoid table not found error.
	 */
	// use DatabaseTransactions;

	// public function setUp()
	// {
	// 	parent::setUp();
	// 	Artisan::call('db:seed', ['--class' => 'Oauth2Seeder']);
	// }

	// /** @test */
	// public function it_returns_token_when_using_oauth_authentication()
	// {
	// 	$user = factory(User::class)->create();

	// 	$response = $this->post('/oauth/token',[
	// 		'grant_type' => 'password',
	// 		'client_id' => 1,
	// 		'client_secret' => 'secret',
	// 		'username' => $user->email,
	// 		'password' => 'password',
	// 		'scope' => '*'
	// 	]);

	// 	$data = json_decode($response->getContent(),true);

	// 	$response
	// 	    ->assertStatus(200)
	// 	    ->assertJson($data);
	// }
	// /** @test */
	// public function it_can_log_users_successfully()
	// {
	// 	$user = factory(User::class)->create();
	// 	$response = $this->post('/oauth/token',[
	// 		'grant_type' => 'password',
	// 		'client_id' => 1,
	// 		'client_secret' => 'secret',
	// 		'username' => $user->email,
	// 		'password' => 'password',
	// 		'scope' => '*'
	// 	]);
	
	// 	$accessToken = json_decode((string)$response->getContent())->access_token;

	// 	$response = $this->json('GET','/user',[],[
	// 			'Accept' => 'application/json',
	// 			'Content-Type' => 'application/json',
	// 			'Authorization' => 'Bearer '.$accessToken
	// 	]);
	// 	$data = json_decode($response->getContent(), true);
	// 	$response->assertStatus(200)
	// 			->assertJson($data);
	// }
}