<?php

namespace Tests\Integration;

use App\Cake;
use App\User;
use Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use Laravel\Passport\Passport;
use Tests\ApiTester;
use Tests\Traits\OauthToken;

class CakesControllerTest extends ApiTester
{

    /** @test */
    public function it_should_return_list_of_cakes()
    {
    	$cakes = factory(Cake::class,10)->create();

    	$response = $this->get('/cakes');

        $data = json_decode($response->getContent(), true);
        $this->assertCount(10, $data['data']);

    }
    /** @test */
    public function it_should_return_a_single_cake()
    {
        $cake = factory(Cake::class)->create();
        
        $response = $this->get('/cakes/'.$cake->slug)
            ->assertStatus(200);

        $data = json_decode($response->getContent(), true);
        $this->assertCount(1, $data);
        
    }
    /** @test */
    public function it_creates_new_cake()
    {
        $cake = factory(Cake::class)->make();
        $user = factory(User::class)->create();
        Passport::actingAs($user);

        $file = $this->prepareFile('cakesnaps-sign-off.png');

        $response = $this->call('POST', '/cakes', [
                'name' => $cake->name,
                'description' => $cake->description,
                'created_at' => Carbon::now(),
            ], [
                'Accept' => 'application/json'
            ],[
            'image' => $file
        ]);

      
        $response->assertStatus(201);
    }

    /** @test */
    public function it_updates_existing_cake_details()
    {   
        $cake = factory(Cake::class)->create();
        Passport::actingAs($cake->user);

        $file = $this->prepareFile('cakesnaps-sign-off.png');

        $response = $this->call('PUT', '/cakes/' .$cake->slug, [
                'name' => $cake->name,
                'description' => $cake->description,
                'created_at' => Carbon::now(),
            ], [],[
            'image' => $file
        ]);



        $response->assertStatus(201);

    }
    /** @test */
    public function it_checks_if_user_is_authorize_to_update_cakes()
    {
        $user1 = factory(User::class)->create();
        $cake = factory(Cake::class)->create();
        Passport::actingAs($user1);
        $file = $this->prepareFile('cakesnaps-sign-off.png');

        $response = $this->call('PUT','/cakes/' .$cake->slug,[
            'name' => $cake->name,
            'description' => $cake->description,
            'picture_url' => $cake->picture_url,
            'user_id' => $cake->user_id,
            'slug' => $cake->slug
        ], [],[
            'image' => $file
        ]);


        //Unauthorize to update cake
        $data = json_decode($response->getContent(), true);
  

        $response->assertStatus(403);
        $this->assertArrayHasKey('errors', $data);
    }

    /** @test */
    public function it_deletes_a_cake()
    {
        $cake = factory(Cake::class)->create();
        Passport::actingAs($cake->user);
        $response = $this->json('delete', '/cakes/' .$cake->slug);

        $response->assertStatus(204);

    }

    /** @test */
    public function it_checks_if_user_is_authorize_to_delete_a_cake()
    {
        $user1 = factory(User::class)->create();
        $cake = factory(Cake::class)->create();

        Passport::actingAs($user1);

        $response = $this->json('delete','/cakes/' .$cake->slug);
        
        //Unauthorize to update cake
        $response->assertStatus(403);
    }
    /** @test */
    public function it_validates_if_image_is_a_valid_format()
    {
        $cake = factory(Cake::class)->make();
        Passport::actingAs(
            factory(User::class)->create()
        );
        $file = $this->prepareFile('invalid-format.pdf');

        $response = $this->call('POST', '/cakes', [
                'name' => $cake->name,
                'description' => $cake->description,
                'created_at' => Carbon::now(),
            ], [
                'Accept' => 'application/json'
            ],[
            'image' => $file
        ]);
        
        $expected = json_decode($response->getContent(), true);
        $response->assertStatus(422);
        $this->assertArrayHasKey('errors', $expected);
    }
    /** @test */
    // public function it_creates_cake_even_there_is_no_image()
    // {
    //     $cake = factory(Cake::class)->make();
                
    //     Passport::actingAs($cake->user);

    //     $response = $this->post('/cakes', [
    //         'name' => $cake->name,
    //         'description' => $cake->description
    //     ],[
    //         'Accept' => 'application/json'
    //     ]);

    //     $data = json_decode($response->getContent(), true);

    //     $response->assertStatus(201);
    //     $this->assertCount(1, $data);
    // }

    /** @test */
    public function it_validates_required_fields()
    {
        Passport::actingAs(
            factory(User::class)->create()
        );
        $response = $this->post('/cakes', [
            'name' => '',
            'description' => ''
        ],[
            'Accept' => 'application/json'
        ]);

        $data = json_decode($response->getContent(), true);
        
        $response
            ->assertJson($data)
            ->assertStatus(422);
    }

}
