<?php

use App\Cake;
use App\Category;
use App\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Laravel\Passport\Passport;
use Tests\ApiTester;

class CategoriesControllerTest extends ApiTester
{
	use DatabaseTransactions;
	/** @test */
	public function it_creates_category()
	{
		Passport::actingAs(
			factory(User::class)->create()
		);
		$category = factory(Category::class)->make();

		$response = $this->post('/categories', [
			'name' => $category->name,
			'description' => $category->description
		]);

		$this->assertCount(1, 
			json_decode($response->getContent(), true) 
		);
		$response->assertStatus(201);
	}
	/** @test */
	public function it_shows_all_categories()
	{
		$categories = factory(Category::class, 20)->create();

		$response = $this->get('/categories');

		$this->assertCount(20, 
			json_decode($response->getContent(), true)['data']
		);
		$response->assertStatus(200);
	}

	/** @test */
	public function it_updates_category()
	{
		Passport::actingAs(
			factory(User::class)->create()
		);
		$category = factory(Category::class)->create();

		$response = $this->put('/categories/' .$category->slug, [
			'name' => 'category-updated',
			'description' => $category->description
		]);
		$data = json_decode($response->getContent(), true);

		$response
			->assertJson($data)
			->assertStatus(201);
	}
	/** @test */
	public function it_validates_required_fields()
	{
		Passport::actingAs(
			factory(User::class)->create()
		);
		$response = $this->post('/categories', [
			'name' => ''
		],[
			'accept' => 'application/json'
		]);
		$data = json_decode($response->getContent(), true);
		$response
			->assertJson($data)
			->assertStatus(422);
	}

	/** @test */
	public function it_shows_category_and_related_cakes()
	{
		Passport::actingAs(
			factory(User::class)->create()
		);
		$cake = factory(Cake::class)->create();
		$cake2 = factory(Cake::class)->create();

		$category = factory(Category::class)->create();
		$category->cakes()->attach([$cake->id, $cake2->id]);
		$response = $this->get('/categories/' .$category->slug);

		$data = json_decode($response->getContent(),true);
		$response->assertStatus(200)
			->assertJson($data);
	}
}