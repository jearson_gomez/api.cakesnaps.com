<?php

namespace Tests\Traits;

use App\User;
use GuzzleHttp\Client;

trait OauthToken
{
	public function getTokenFrom(User $user)
	{
		$http = new Client;
		$response = $http->post('http://api.cakesnaps.com/oauth/token', [
			"form_params" => [
				"grant_type" => "password",
				"client_id" => 1,
				"client_secret" => "B0V2coDjDLC1ScuY33FjHiDip8F42jiYrYfFLq1T",
				"username" => "jearsonbgomez@yahoo.com",
				"password" => "password",
				"scope" => "*"
			]
		]);
	
		return json_decode((string)$response->getBody(), true);
	}
}