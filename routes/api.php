<?php

use Illuminate\Http\Request;

Route::prefix('/cakes/categories')->group(function () {
	Route::name('categories.store')->post('/', 'CategoriesController@store');
	Route::name('categories.show')->get('/{category}', 'CategoriesController@show');
	Route::name('categories.update')->put('/{category}', 'CategoriesController@update');
	Route::name('categories.index')->get('/', 'CategoriesController@index');
});


Route::prefix('/cakes/themes')->group(function () {
	Route::name('themes.store')->post('/', 'ThemesController@store');
	Route::name('themes.show')->get('/{theme}', 'ThemesController@show');
	Route::name('themes.update')->put('/{theme}', 'ThemesController@update');
	Route::name('themes.destroy')->delete('/{theme}', 'ThemesController@delete');
	Route::name('themes.index')->get('/', 'ThemesController@index');
});

Route::prefix('/cakes')->group(function () {
	Route::name('cakes.store')->post('/', 'CakesController@store');
	Route::name('cakes.show')->get('/{cake}', 'CakesController@show')->where('cake', '^[0-9a-zA-Z_.-]*$');
	Route::name('cakes.update')->put('/{cake}', 'CakesController@update');
	Route::name('cakes.destroy')->delete('/{cake}', 'CakesController@destroy');
	Route::name('cakes')->get('/', 'CakesController@index');
	Route::name('cakes.uploadImage')->post('/{cakes}/image', 'CakesController@uploadImage');
	Route::name('cakes.comments.store')->post('/{cakes}/comments', 'CommentsController@store');
	Route::name('cakes.comments.show')->get('/{cakes}/comments', 'CommentsController@show');	
	Route::name('cakes.categories')->post('/{cakes}/categories', 'CakesController@link');
	Route::delete('/{cakes}/categories', 'CakesController@unlink');

});
Route::post('/register', 'RegisterController@register');
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/notifications', 'UsersController@notifications');
Route::get('/unread-notifications', 'UsersController@unreadNotifications');

Route::post('login/facebook', 'SocialLoginController@redirectToProvider');
// Route::get('login/facebook/callback', 'SocialLoginController@handleProviderCallback');

Route::prefix('/users')->group(function () {
	Route::name('users.update')->put('/{user}', 'UsersController@update');
	Route::name('users.destroy')->delete('/{user}', 'UsersController@delete');
	Route::name('users.uploadImage')->patch('/{user}/image', 
		'UsersController@uploadImage');
	Route::name('users.index')->get('/', 'UsersController@index');
});




