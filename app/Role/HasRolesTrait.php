<?php

namespace App\Role;

trait HasRolesTrait
{
	/**
	 * Users belongs to many roles
	 * 
	 * @return App\Role
	 */
	public function roles()
	{
		return $this->belongsToMany(Role::class);
	}
	public function hasRoles(...$role)
	{
		dd($role);
	}
}