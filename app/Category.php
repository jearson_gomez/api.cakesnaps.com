<?php

namespace App;

use App\Scopes\ModelScope;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
	use ModelScope;

	/**
	 * Will disable timestamps created_at and updated_at
	 * 
	 * @var timestamps
	 */
	public $timestamps = false;

	/**
	 * Fields that are mass assignable
	 * 
	 * @var array
	 */
	protected $fillable = [
		'name' , 'description','slug'
	];
	
   	/**
	 * Category belongs to many cakes
	 * 
	 * @return App\Cake
	 */
    public function cakes()
    {
    	return $this->belongsToMany(Cake::class);
    }

   	/**
	 * Get the route key for the model.
	 *
	 * @return string
	 */
	public function getRouteKeyName()
	{
	    return 'slug';
	}

}
