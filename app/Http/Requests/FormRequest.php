<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest as IlluminateFormRequest;
use Illuminate\Http\JsonResponse;

class FormRequest extends IlluminateFormRequest
{
    /**
     * Get the proper failed validation response for the request.
     *
     * @param  array  $errors
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function response(array $errors)
    {
        
        return new JsonResponse([
            'errors' => $errors
        ], 422);
        parent::response($errors);
    }
}
