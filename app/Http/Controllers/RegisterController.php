<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreUserRequest;
use App\Transformers\Traits\TransformerTrait;
use App\Transformers\UserTransformer;
use App\User;

class RegisterController extends Controller
{
    use TransformerTrait;
	/**
	 * Handles registration of users on api
	 *
	 * @param Illuminate\Http\Request $request
	 * @return Illuminate\Http\Response
	 */
    public function register(StoreUserRequest $request)
    {

    	$user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'gender' => $request->gender ?: null,
            'timezone' => $request->timezone ?: null,
            'location' => $request->location ?: null,
            'password' => bcrypt($request->password),
            'thumbnail' => $request->avatar,
            'bio' => $request->bio ?: null
        ]);
        
        return $this->transformResponse($user, new UserTransformer);
    }
}
