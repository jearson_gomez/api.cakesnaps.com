<?php

namespace App\Http\Controllers;

use App\Transformers\Traits\TransformerTrait;
use App\User;
use Illuminate\Http\Request;
use App\Transformers\UserTransformer;
use Laravel\Socialite\Facades\Socialite;

class SocialLoginController extends Controller
{
	use TransformerTrait;
	/**
     * Redirect the user to the GitHub authentication page.
     *
     * @return Response
     */
    public function redirectToProvider(Request $request)
    {

        return $this->createOrReturnExistingUser($request);

    }

    /**
     * Obtain the user information from Facebook.
     *
     * @return Response
     */
    public function createOrReturnExistingUser(Request $request)
    {
        $user = User::where('email', $request->email)->first();

        if($user)
        {
            return $this->transformResponse($user, new UserTransformer);
        }

        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'gender' => $request->gender ?: null,
            'timezone' => $request->timezone ?: null,
            'location' => $request->location ?: null,
            'password' => bcrypt($request->password),
            'thumbnail' => $request->avatar,
            'picture_url' => $request->avatar,
            'bio' => $request->bio ?: null
        ]);

        return $this->transformResponse($user, new UserTransformer);
   
    }
}
