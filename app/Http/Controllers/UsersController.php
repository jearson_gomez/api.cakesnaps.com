<?php

namespace App\Http\Controllers;

use App\Http\Requests\UpdateUserRequest;
use App\Transformers\Traits\TransformerTrait;
use App\Transformers\UserTransformer;
use App\User;
use App\UserForm;
use Illuminate\Http\Request;

class UsersController extends Controller
{
    use TransformerTrait;

    /**
     * 
     * @var userForm
     */
    protected $userForm;

    /**
     * Instantiate user
     * 
     * @param User $user 
     */
    public function __construct(UserForm $userForm)
    {
        $this->middleware('auth:api', ['except' => ['index']]);
        $this->userForm = $userForm;
    }
   	/**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(User $user)
    {

        $paginator = $user->latestFirst()->paginate(20);
        $users = $paginator->getCollection();

        return $this->transformCollection($users, new UserTransformer, $paginator, ['cakes']);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateUserRequest $request, User $user)
    {
        $user->update($request->all());
        return $this->transformResponse($user, new UserTransformer, ['cakes']);
    }

    public function notifications(Request $request)
    {
        return response()->json([
            'data' => $request->user()->notifications
        ]);
    }

    public function unreadNotifications(Request $request)
    {
        return response()->json([
            'data' => $request->user()->unreadNotifications,
            'count' => count($request->user()->unreadNotifications)
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Update image of user from storage
     * 
     * @return \Illuminate\Http\Response
     */
    public function uploadImage(Request $request, User $user)
    {
        $this->userForm->save($request, $user);

        return $this->transformResponse($user, new UserTransformer, ['cakes']);
    }
}
