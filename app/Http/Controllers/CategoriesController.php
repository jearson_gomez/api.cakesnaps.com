<?php

namespace App\Http\Controllers;

use App\Cake;
use App\Category;
use Illuminate\Http\Request;
use App\Http\Requests\CategoryRequest;
use App\Transformers\CategoryTransformer;
use App\Transformers\Traits\TransformerTrait;

class CategoriesController extends Controller
{
    use TransformerTrait;

    public function __construct()
    {
        $this->middleware('auth:api',['only' => ['store','update']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Category $category)
    {
        $paginator = $category->alphabeticalFirst()->paginate(20);
        $categories= $paginator->getCollection();



        return $this->transformCollection($categories, new CategoryTransformer, $paginator);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CategoryRequest $request, Category $category)
    {
        $category->create(
            $request->all()
        );
        
        return $this->transformResponse(
            $category, new CategoryTransformer
        );
    }

    /**
     * Display a single category
     * 
     * @param  Category $category
     * @return Response
     */
    public function show(Category $category)
    {

        return $this->transformItem($category, new CategoryTransformer,['cakes.user', 'cakes.categories']);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CategoryRequest $request, Category $category)
    {
 
        $category->update($request->all());

        return $this->transformResponse($category, new CategoryTransformer);
    }
}
