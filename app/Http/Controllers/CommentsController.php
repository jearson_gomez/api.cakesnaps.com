<?php

namespace App\Http\Controllers;

use App\Cake;
use App\Comment;
use Illuminate\Http\Request;
use App\Transformers\Traits\TransformerTrait;
use App\Transformers\CommentTransformer;
use Pusher;
use App\Notifications\CakeNotification;


class CommentsController extends Controller
{
	use TransformerTrait;

	public function __construct()
	{
		$this->middleware('auth:api', ['only' => ['store']]);
	}

    public function show($slug)
    {

    	$cake = Cake::where('slug', $slug)->first();

    	$paginator = Comment::where('cake_id', $cake->id)->paginate(10);

    	$comments = $paginator->getCollection();



    	return $this->transformCollection($comments, new CommentTransformer, $paginator, ['user','cake']);
    }

    public function store($slug, Request $request)
    {


    	$cake = Cake::where('slug', $slug)->first();


    	$comment = $request->user()->comments()->create([
    		'comment' => $request->comment,
    		'cake_id' => $cake->id
    	]);

    	//instantiate pusher
        //or better to use pusher package for laravel
    	$options = array(
		    'cluster' => env('PUSHER_CLUSTER'),
		    'encrypted' => true
		);

        //refactor this later

		$pusher = new Pusher(
		    env('PUSHER_APP_KEY'),
		    env('PUSHER_APP_SECRET'),
		    env('PUSHER_APP_ID'),
		    $options
		);

    	//get message

    	$data = $this->transformItem($comment, new CommentTransformer,['user', 'cake']);


        $private = 'cakesnaps-channel.' .$cake->user->email;

        $notification['data'] =  [
            'title' => "CakeSnaps Notification",
            'subtitle' => $cake->name,
            'body' => $request->comment,
            'thumbnail' => $request->user()->picture_url

        ];

        //database notification
        $cake->user->notify(
            new CakeNotification(
                $request->comment, 
                $request->user()->name, 
                $cake->picture_url,
                $cake->name
            )
        );

        $dbNotification = [
            'message' => $request->comment, 
            'user_name'  => $request->user()->name, 
            'cake_picture_url' => $cake->picture_url,
            'cake_name'  =>  $cake->name
        ];


        //used for notification on ios phone not on app
        //notification on mobile phone
        //Better to use client pusher for mobile
        //Did not use here since Im getting error on the package
        //And have no time to debug
        $pusher->trigger($private, 'cakesnaps-comment-local', $notification);

        $pusher->trigger($private, 'db-notification', $dbNotification);




    	//broadcast message
        //real time notification on app
    	$pusher->trigger('cakesnaps-channel', 'cakesnaps-event', $data);

        $pusher->trigger('cakesnaps-channel', 'comments-count', ['count' => count($cake->user->unreadNotifications)]);

    	return $data;


    }
}
