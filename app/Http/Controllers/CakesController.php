<?php

namespace App\Http\Controllers;

use App\CakeForm;
use App\User;
use App\Cake;
use App\Like;
use App\Category;
use App\Theme;
use App\Notifications\CakeNotification;
use Pusher;
use Illuminate\Http\Request;
use App\Http\Requests\CakeRequest;
use App\Http\Requests\UpdateCakeRequest;
use App\Http\Requests\UploadImageRequest;
use App\Transformers\CakeTransformer;
use App\Transformers\Traits\TransformerTrait;
use App\Notifications\CakeHasBeenLike;

class CakesController extends Controller
{
    use TransformerTrait;

    /**
     * 
     * @var cakeFormService
     */
    protected $cakeFormService;

    /**
     * Instantiate class
     * 
     * @param CakeForm $cakeFormService
     */
    public function __construct(CakeForm $cakeFormService)
    {
        $this->cakeFormService = $cakeFormService;
        $this->middleware('auth:api',['only' => ['store','update', 'destroy']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $total = $request->items;

        $paginator = Cake::latestFirst()->paginate($total);
        $cakes = $paginator->getCollection();


        if($request->getQueryString() == 'me'){

            if( !is_null($user = $request->user()) ){

                $paginator = $user->cakes()->paginate($total);
                $cakes = $paginator->getCollection();
            }
        }


        return $this->transformCollection($cakes, new CakeTransformer,
            $paginator, ['user','categories','likes.user']
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CakeRequest $request)
    {

        $cake = $this->cakeFormService->create($request);

        if($slug = $request->theme){

            $theme = Theme::where('slug', $slug)->first();

            $cake->themes()->attach($theme->id);
        }
        
        return $this->transformResponse($cake, new CakeTransformer, ['user']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $cake
     * @return \Illuminate\Http\Response
     */
    public function show(Cake $cake, Request $request)
    {
        if( $request->has('likes') && !is_null($user = $request->user()) ){

            $options = array(
                'cluster' => env('PUSHER_CLUSTER'),
                'encrypted' => true
            );
            //refactor this pusher later on to avoid redunduncy will use service provider or package
            //or better to use pusher package for laravel
            $pusher = new Pusher(
                env('PUSHER_APP_KEY'),
                env('PUSHER_APP_SECRET'),
                env('PUSHER_APP_ID'),
                $options
            );
            if($request->likes){

                $cake->like();

                //refactor later
                //notification on mobile phone
                //Better to use client pusher for mobile
                //Did not use here since Im getting error on the package
                //And have no time to debug
                $private = 'cakesnaps-channel.' .$cake->user->email;

                if($cake->user->id != $user->id){
                    $notification['data'] =  [
                        'title' => "CakeSnaps Notification",
                        'subtitle' => $cake->name,
                        'body' => "{$user->name}  likes your cake.",
                        'thumbnail' => $user->picture_url

                    ];

                    $message = $user->name . ' likes ' . $cake->name;
                    
                    //push to database notification
                    $cake->user->notify(
                        new CakeNotification(
                            $message,
                            $user->name,
                            $cake->picture_url,
                            $cake->name
                        )
                    );

                    $dbNotification = [
                        'message' => $message, 
                        'user_name'  => $request->user()->name, 
                        'cake_picture_url' => $cake->picture_url,
                        'cake_name'  =>  $cake->name
                    ];


                    //notification on mobile phone
                    //Better to use client pusher for mobile
                    //Did not use here since Im getting error on the package
                    //And have no time to debug
                    $pusher->trigger($private, 'like-notification', $notification);

                    $pusher->trigger($private, 'db-notification', $dbNotification);
                }
                
            }else {
                $cake->unlike();
            }

            $data['like'] = [
                'id' => $cake->id, 
                'liked_by_current_user' => $cake->liked(),
                'total_likes' => $cake->total_likes,
                'unread' => count($cake->user->unreadNotifications)
            ];

            //broadcast message
            $pusher->trigger('cakesnaps-channel', 'like-unlike-cake', $data);

        }

        return $this->transformItem($cake, new CakeTransformer, ['user','categories','likes.user']);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateCakeRequest $request, Cake $cake)
    {
        $this->authorize('update', $cake);

        $cake = $this->cakeFormService->update($request, $cake);
        return $this->transformResponse($cake, new CakeTransformer, ['user']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Cake $cake)
    {
      
        $flag = $this->authorize('delete', $cake);
        
        $cake->delete();

        return response(null, 204);
    }

    /**
     * Update image of cake from storage
     * 
     * @return \Illuminate\Http\Response
     */
    public function uploadImage(UploadImageRequest $request, $slug)
    {
        $cake = Cake::where('slug', $slug)->first();

        $this->authorize('update', $cake);

        $cake = $this->cakeFormService->updateImage($request, $cake);

        return $this->transformResponse($cake, new CakeTransformer, ['user']);
    }

    //need to extract this to another controller
    public function link($slug, Request $request)
    {
        $cake = Cake::where('slug', $slug)->first();

        $category = Category::where('name', $request->category)->first();

        //refactor this code later
        //or better to use pusher package for laravel
        $options = array(
            'cluster' => env('PUSHER_CLUSTER'),
            'encrypted' => true
        );

        $pusher = new Pusher(
            env('PUSHER_APP_KEY'),
            env('PUSHER_APP_SECRET'),
            env('PUSHER_APP_ID'),
            $options
        );

        $cake->categories()->attach($category->id);

        $data["links"] = $request->category;

        //broadcast message
        $pusher->trigger('cakesnaps-channel', 'link-cake', $data);

        return $this->transformItem($cake, new CakeTransformer, ['user','categories']);
    }

    //need to extract this to another controller
    public function unlink($slug, Request $request)
    {

        $cake = Cake::where('slug', $slug)->first();

        $category = Category::where('name', $request->category)->first();

                        //instantiate pusher
        $options = array(
            'cluster' => env('PUSHER_CLUSTER'),
            'encrypted' => true
        );

        //refactor this pusher later on to avoid redunduncy
        //or better to use pusher package for laravel
        $pusher = new Pusher(
            env('PUSHER_APP_KEY'),
            env('PUSHER_APP_SECRET'),
            env('PUSHER_APP_ID'),
            $options
        );

        $cake->categories()->detach($category->id);

        $data["unlinks"] = $request->category;

        //broadcast message
        $pusher->trigger('cakesnaps-channel', 'unlink-cake', $data);

        return $this->transformItem($cake, new CakeTransformer, ['user','categories']);
    }

}
