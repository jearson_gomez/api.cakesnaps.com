<?php

namespace App\Http\Controllers;

use App\Theme;
use App\ThemeForm;
use Illuminate\Http\Request;
use App\Http\Requests\ThemeRequest;
use App\Transformers\ThemeTransformer;
use App\Transformers\Traits\TransformerTrait;

class ThemesController extends Controller
{
    use TransformerTrait;

    protected $themFormService;

    public function __construct(ThemeForm $themFormService)
    {
        $this->themeFormService = $themFormService;
        $this->middleware('auth:api', ['only' => ['store', 'update']]);
        
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Theme $theme)
    {

        $paginator = $theme->alphabeticalFirst()->paginate(20);
        $themes = $paginator->getCollection();

        return $this->transformCollection($themes, new ThemeTransformer, $paginator);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ThemeRequest $request)
    {

        $theme = $this->themeFormService->create($request);
        
        return $this->transformResponse( $theme, new ThemeTransformer , ['user']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Theme $theme)
    {
        return $this->transformItem($theme, new ThemeTransformer,[
            'cakes.user', 
            'cakes.categories',
            'winner.user',
            'winner.categories'
        ]);
    }



    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
