<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Like extends Model
{
	protected $fillable = ['likeable_id', 'likeable_type', 'user_id'];
	/**
	 * Get all of the owning likeable model
	 */
    public function likeable()
    {
    	return $this->morphTo();
    }

}
