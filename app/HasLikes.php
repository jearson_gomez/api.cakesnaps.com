<?php

namespace App;
use Auth;
use Illuminate\Http\Request;
use App\Like;

trait HasLikes
{

	/**
     * Get all of the cake's likes
     */
    public function likes()
    {
        return $this->morphMany(Like::class, 'likeable');
    }

    /**
     * Count all likes for cake
     */
    public function getTotalLikesAttribute($value)
    {
        return $this->likes()->count();
    }

    /**
     * Will like a cake
     */
    public function like()
    {
    	

    	$like = new Like();
    	$like->user_id = Auth::user()->id;
    	$this->likes()->save($like);

    }

    /**
     * Will check if current user already liked a cake
     *
     * @return Boolean
     */
    public function liked()
    {

    	if(Auth::check()){

    		return (bool) 
    		$this->likes()
    		->where('user_id', Auth::user()->id)
    		->count();
    	}

    	return false;
    }

    public function unlike()
    {
    	$like = $this->likes()->where('user_id', Auth::user()->id)->first();

    	$like->delete();
    }
 
}