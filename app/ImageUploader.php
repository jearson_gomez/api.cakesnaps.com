<?php

namespace App;

use App\Cake;
use App\Thumbnail;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class ImageUploader
{
	/**
	 * 
	 * @var thumbnail
	 */
	public $thumbnail;

	/**
	 * Instantiate Class
	 * 
	 * @param Thumbnail|null $thumbnail 
	 */
	public function __construct(Thumbnail $thumbnail = null)
	{
		$this->thumbnail = $thumbnail ? : new Thumbnail;
	}

	/**
	 * Will stream uploaded image on temporary storage
	 * 
	 * @param  Cake   $cake  
	 * @param  file $image 
	 * @return string
	 */
	public function makePhoto(Model $model, $image)
	{
		$imageId = $this->generateId($image);
		$model->addPhoto($imageId);
		return $imageId;
	}

	/**
	 * It generates random id for image
	 * 
	 * @param  file $file 
	 * @return Response
	 */
	public function generateId($file)
	{
		return Storage::putFileAs('images', $file, uniqid(true));
	}

	/**
	 * Save path of image being uploaded
	 * 
	 * @param  Model   $cake    
	 * @param  string $imageId 
	 * @return Response
	 */
	public function saveImage(Model $model, $imageId)
	{

		$upload = $this->upload($model, $imageId);

		if($upload){
			$this->removeImageFromStorage($imageId);
		}
		return response()->json([],201);
	}

	/**
	 * Will delete streamed image in storage
	 * 
	 * @param  File $imageId
	 * @return void
	 */
	public function removeImageFromStorage($imageId)
	{
		Storage::delete($imageId);
		Storage::delete($imageId .'-th');
	}
	/**
	 * Will upload image to s3Amazon
	 * 
	 * @param  File $imageId 
	 * @return void
	 */
	public function upload(Model $model, $imageId)
	{
		Storage::disk('s3')->put(
			'/' .$imageId .'.png', 
			fopen($model->pictureBaseDir() .$imageId, 'r+'
		));
		Storage::disk('s3')->put(
			'/' .$imageId .'-th.png', 
			fopen($model->pictureBaseDir() .$imageId .'-th', 'r+'
		));
		return true;
	}
}