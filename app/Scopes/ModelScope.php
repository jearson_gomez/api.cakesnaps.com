<?php
namespace App\Scopes;

trait ModelScope
{
    /**
     * Scope a query to fetch latest data.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
	public function scopeLatestFirst($query)
	{
		return $query->orderBy('created_at', 'desc');
	}
	/**
     * Scope a query to fetch data alphabetically.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
	public function scopeAlphabeticalFirst($query)
	{
		return $query->orderBy('name', 'desc');
	}
 

}