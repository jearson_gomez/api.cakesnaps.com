<?php

namespace App;

use App\Scopes\ModelScope;
use Illuminate\Database\Eloquent\Model;
use Storage;

class Theme extends Model
{
	use ModelScope;

	protected $fillable = ['name', 'description', 'slug', 'picture_url','end_of_competition'];
	
	/**
	 * Theme belongs to many cakes
	 * 
	 * @return App\Category
	 */
    public function cakes()
    {
    	return $this->belongsToMany(Cake::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Root path of theme's image
     * 
     * @return string
     */
    public function pictureBaseDir()
    {
        return storage_path() .'/app/';
    }

    /**
     * Set path of thumbnail and picture url
     * 
     * @param void
     */
    public function setPicFileNameAttribute($value)
    {
        $this->thumbnail = Storage::disk('s3')->url($value .'-th.png');
        $this->picture_url = Storage::disk('s3')->url($value .'.png');
    }


    public function getPicFileNameAttribute($value)
    {
        return [
            'thumbnail' => Storage::disk('s3')->url($value .'-th.png'),
            'picture_url' => Storage::disk('s3')->url($value .'.png')
        ];
    }

    /**
     * Will add image to thumnail and picture_url attribute
     * @param string $imageId 
     */
    public function addPhoto($imageId)
    {
        $this->pic_file_name = $imageId;
        $this->save();
    }

    /**
	 * Get the route key for the model.
	 *
	 * @return string
	 */
	public function getRouteKeyName()
	{
	    return 'slug';
	}
}
