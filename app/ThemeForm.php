<?php

namespace App;

use App\Theme;
use App\ImageUploader;
use App\Jobs\UploadThemesImage;
use App\Http\Requests\ThemeRequest;
use Carbon\Carbon;


class ThemeForm extends ImageUploader
{
	/**
	 * Will create new instance of theme
	 *
	 * @param ThemeRequest
	 * @return Response
	 */
	public function create(ThemeRequest $request)
	{

		$date = Carbon::createFromFormat('m/d/y', $request->end_of_competition)->toDateString();

		$theme = $request->user()->createCakeFight(
			new Theme([
				'name' => $request->name,
				'description' => $request->description,
				'end_of_competition' => $date
			])
		);

		$this->queueImage($request, $theme);

		return $theme;
	}

	public function queueImage(ThemeRequest $request, Theme $theme)
	{
		if($request->hasFile('image'))
		{
			$imageId = $this->makePhoto($theme, $request->image);
			
			$this->thumbnail->make($theme->pictureBaseDir() .$imageId);

			dispatch(new UploadThemesImage($imageId, $theme));
		}
		return null;
	}
}