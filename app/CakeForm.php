<?php
namespace App;

use App\Cake;
use App\ImageUploader;
use App\Http\Requests\CakeRequest;
use App\Http\Requests\UpdateCakeRequest;
use App\Http\Requests\UploadImageRequest;
use App\Jobs\UploadCakesImage;

class CakeForm extends ImageUploader
{
	/**
	 *  Will create new Cake
	 *  
	 * @param  CakeRequest $request 
	 * @return Response
	 */
	public function create(CakeRequest $request)
	{
		$cake = $request->user()->snaps(
            new Cake($request->all())
        );
        $this->save($request, $cake);
        return $cake;
	}

	/**
	 * Will queu uploading of image 
	 * 
	 * @param  CakeRequest|UploadCakeRequest $request 
	 * @param  Cake        $cake    
	 * @return void     
	 */
	public function save($request, Cake $cake)
	{
		if($request->hasFile('image'))
		{
			$imageId = $this->makePhoto($cake, $request->image);
			
			$this->thumbnail->make($cake->pictureBaseDir() .$imageId);

			dispatch(new UploadCakesImage($imageId, $cake));
		}
		return null;
	}

	/**
	 * A Service for updating cakes form details
	 * 
	 * @param  CakeRequest $request 
	 * @param  Cake        $cake    
	 * @return Response
	 */
	public function update(UpdateCakeRequest $request, Cake $cake)
	{
		$cake->update($request->all());

		// $this->save($request, $cake);
		
		return $cake;
	}

	public function updateImage(UploadImageRequest $request, Cake $cake)
	{
		$this->save($request, $cake);

		return $cake;
	}

}