<?php

namespace App\Permission;

use App\Permission;

trait HasPermissionsTrait
{
	/**
	 * User belongs to many permissions
	 * 
	 * @return App\Permission
	 */
	public function permissions()
	{
		return $this->belongsToMany(Permission::class);
	}
}