<?php

namespace App\Uuid;
use Webpatser\Uuid\Uuid;

trait UuidModel
{
	/**
	 * Boot function for laravel
	 * @return void
	 */
	protected static function boot()
	{
		parent::boot();
		static::creating(function ($model) {
			$model->{$model->getKeyName()} = Uuid::generate(4)->string;
		});
	}
}