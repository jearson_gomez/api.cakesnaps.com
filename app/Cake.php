<?php

namespace App;

use App\Scopes\ModelScope;
use App\User;
use App\Uuid\UuidModel;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use App\HasLikes;



class Cake extends Model
{
    use UuidModel, ModelScope, HasLikes;


    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;

    /**
     * Mass assignable fields
     * 
     * @var array
     */
    protected $fillable = [
        'name', 'description', 'user_id', 'picture_url'
    ];


	/**
	 * Cake belongs to many categories
	 * 
	 * @return App\Category
	 */
    public function categories()
    {
    	return $this->belongsToMany(Category::class);
    }
    /**
     * Cake belongs to many themes
     * 
     * @return App\Theme
     */
    public function themes()
    {
    	return $this->belongsToMany(Theme::class);
    }
    /**
     * Cake belongs to one user
     * 
     * @return App\User
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function comments()
    {
        return $this->hasMany(Comment::class);
    }


    /**
     * Root path of cake's image
     * 
     * @return string
     */
    public function pictureBaseDir()
    {
        return storage_path() .'/app/';
    }

    /**
     * Set path of thumbnail and picture url
     * 
     * @param void
     */
    public function setPicFileNameAttribute($value)
    {
        $this->thumbnail = Storage::disk('s3')->url($value .'-th.png');
        $this->picture_url = Storage::disk('s3')->url($value .'.png');
    }


    public function getPicFileNameAttribute($value)
    {
        return [
            'thumbnail' => Storage::disk('s3')->url($value .'-th.png'),
            'picture_url' => Storage::disk('s3')->url($value .'.png')
        ];
    }

    /**
     * Will add image to thumnail and picture_url attribute
     * @param string $imageId 
     */
    public function addPhoto($imageId)
    {
        $this->pic_file_name = $imageId;
        $this->save();
    }


    /**
     * Get the route key for the model
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'slug';
    }

}
