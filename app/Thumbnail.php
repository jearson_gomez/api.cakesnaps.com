<?php
namespace App;

use Image;

class Thumbnail
{
	public function make($path)
	{
		return Image::make($path)->encode('png')->fit(200, 200, function($constraint){
			$constraint->upsize();
		})->save($path .'-th');
	}
}