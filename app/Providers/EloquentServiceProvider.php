<?php

namespace App\Providers;

use App\Cake;
use App\Category;
use App\Theme;
use App\Observers\CakeObserver;
use App\Observers\CategoryObserver;
use App\Observers\ThemeObserver;
use Illuminate\Support\ServiceProvider;

class EloquentServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        Cake::observe(CakeObserver::class);
        Category::observe(CategoryObserver::class);
        Theme::observe(ThemeObserver::class);
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
