<?php

namespace App\Policies;

use App\Cake;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class CakePolicy
{
    use HandlesAuthorization;

    /**
     * Will check if user can update a cake
     *
     * @param  User   $user 
     * @param  Cake   $cake
     * @return boolean
     */
    public function update(User $user, Cake $cake)
    {
        return $user->ownsCake($cake);
    }

    /**
     * Will check if user can delete a cake
     * @param User $user
     * @param Cake $cake
     * @return boolean
     */
    public function delete(User $user, Cake $cake)
    {
        return $user->ownsCake($cake);
    }
}
