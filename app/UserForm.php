<?php

namespace App;

use App\ImageUploader;
use App\Jobs\UploadUsersImage;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class UserForm extends ImageUploader
{
	/**
	 * Will instantaite uploading of image to s3Amazon
	 * 
	 * @param  Request $request 
	 * @param  User    $user    
	 * @return void | null
	 */
	public function save(Request $request, User $user)
	{
		$this->validator($request);

		$user->name = $request->get('name');
		$user->email = $request->get('email');
		$user->password = $request->get('password');

		if($request->hasFile('image'))
		{
			$imageId = $this->makePhoto($user, $request->image);
			$this->thumbnail->make($user->pictureBaseDir() .$imageId);

			dispatch(new UploadUsersImage($imageId, $user));
		}
		return null;
	}

	/**
	 * Validates image input
	 * @param  array  $data
	 * @return Illuminate\Support\Facades\Validator
	 */
	public function validator(Request $request)
	{
		$userId = $request->user()->id;
		return Validator::make($request->all(), [
			'image' => 'required|image',
			'email' => [
				'required',
				Rule::unique('users')->ignore($userId)
			]
		]);
	}
}