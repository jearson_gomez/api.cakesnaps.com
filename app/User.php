<?php

namespace App;

use App\Cake;
use App\Permission\HasPermissionsTrait;
use App\Role\HasRolesTrait;
use App\Scopes\ModelScope;
use App\Uuid\UuidModel;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use Notifiable, HasPermissionsTrait, HasRolesTrait, HasApiTokens, UuidModel, ModelScope;

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'active','gender','timezone','location','picture_url',
        'thumbnail', 'bio','birthdate'
    ];

    protected $appends = [
        'db_notifications'
    ];
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * User has many cakes
     * 
     * @return App\Cake
     */
    public function cakes()
    {
        return $this->hasMany(Cake::class)->latestFirst();
    }

    public function themes()
    {
        return $this->hasMany(Theme::class);
    }

    public function comments()
    {
        return $this->hasMany(Comment::class);
    }


    /**
     * Will create a avatar based on user email
     * 
     * @return string
     */
    public function getAvatar()
    {
        return 'https://www.gravatar.com/avatar/' . md5( strtolower( trim($this->email) ) ) . '?s=45&d=mm';
    }

    /**
     * Compare if cakes id is the same with owner's cake id
     * @param  Cake   $cake 
     * @return boolean
     */
    public function ownsCake(Cake $cake)
    {
        return $this->id == $cake->user_id;
    }

    /**
     * Will capture cake and associate to user
     * 
     * @param  Cake   $cake
     * @return Response
     */
    public function snaps(Cake $cake)
    {
        return $this->cakes()->save($cake);
    }

    public function createCakeFight(Theme $theme)
    {
        return $this->themes()->save($theme);
    }

    /**
     * Root path of user's image
     * 
     * @return string
     */
    public function pictureBaseDir()
    {
        return storage_path() .'/app/';
    }

    /**
     * Set path of thumbnail and picture url
     * 
     * @param void
     */
    public function setPicFileNameAttribute($value)
    {
        $this->thumbnail = $value .'-th.png';
        $this->picture_url = $value .'.png';
    }

    /**
     * Will add image to thumnail and picture_url attribute
     * @param string $imageId 
     */
    public function addPhoto($imageId)
    {
        $this->pic_file_name = $imageId;
        $this->save();
    }

    public function getDbNotificationsAttribute()
    {
        return $this->notifications;
    }

}
