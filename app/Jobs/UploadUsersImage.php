<?php

namespace App\Jobs;

use App\ImageUploader;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class UploadUsersImage implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * 
     * @var user
     */
    protected $user;

    /**
     * 
     * @var imageId
     */
    protected $imageId;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($imageId, User $user)
    {
        $this->imageId = $imageId;
        $this->user = $user;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(ImageUploader $uploader)
    {
        $uploader->saveImage(
            $this->user,
            $this->imageId
        );
    }
}
