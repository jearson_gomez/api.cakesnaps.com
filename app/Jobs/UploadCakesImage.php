<?php

namespace App\Jobs;

use App\Cake;
use App\ImageUploader;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class UploadCakesImage implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * 
     * @var cake
     */
    protected $cake;

    /**
     * 
     * @var imageId
     */
    protected $imageId;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($imageId, Cake $cake)
    {
        $this->imageId = $imageId;
        $this->cake = $cake;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(ImageUploader $uploader)
    {
        $uploader->saveImage(
            $this->cake,
            $this->imageId
        );
    }
}
