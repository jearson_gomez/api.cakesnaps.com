<?php

namespace App\Jobs;

use App\Theme;
use App\ImageUploader;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;



class UploadThemesImage implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * 
     * @var $theme
     */
    protected $theme;

    /**
     *
     * @var $imageId
     */
    protected $imageId;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($imageId, Theme $theme)
    {
        $this->imageId = $imageId;
        $this->theme= $theme;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(ImageUploader $uploader)
    {
        $uploader->saveImage(
            $this->theme,
            $this->imageId
        );
    }
}
