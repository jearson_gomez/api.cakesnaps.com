<?php
namespace App\Observers;
use App\Theme;

class ThemeObserver
{
	/**
	 * Listen to the Theme creating event
	 * 
	 * @param  Category   $category
	 * @return void
	 */
	public function creating(Theme $theme)
	{
        $theme->slug = str_slug($theme->name);
	}

	/**
	 * Listen to the Theme updating event
	 * 
	 * @param  Cake   theme 
	 * @return void
	 */
	public function updating(Theme $theme)
	{
        $theme->slug = str_slug($theme->name);
	}
}