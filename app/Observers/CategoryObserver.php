<?php
namespace App\Observers;

use App\Category;

class CategoryObserver
{
	/**
	 * Listen to the Category creating event
	 * 
	 * @param  Category   $category
	 * @return void
	 */
	public function creating(Category $category)
	{
        $category->slug = str_slug($category->name);
	}

	/**
	 * Listen to the Cake updating event
	 * 
	 * @param  Cake   category 
	 * @return void
	 */
	public function updating(Category $category)
	{
        $category->slug = str_slug($category->name);
	}
}