<?php
namespace App\Observers;

use App\Cake;

class CakeObserver
{
	/**
	 * Listen to the Cake creating event
	 * 
	 * @param  Cake   $cake 
	 * @return void
	 */
	public function creating(Cake $cake)
	{
        $cake->slug = str_slug($cake->name);
	}

	/**
	 * Listen to the Cake updating event
	 * 
	 * @param  Cake   $cake 
	 * @return void
	 */
	public function updating(Cake $cake)
	{
        $cake->slug = str_slug($cake->name);
	}
}