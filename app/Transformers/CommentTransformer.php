<?php

namespace App\Transformers;

use App\Cake;
use App\User;
use App\Comment;
use App\Transformers\CakeTransformer;
use League\Fractal\TransformerAbstract;


class CommentTransformer extends TransformerAbstract
{
	/**
     * List of resources to be included
     * 
     * @var array
     */
    protected $availableIncludes = [
        'user','cake'
    ];

	/**
     * A Fractal transformer.
     *
     * @return array
     */
	public function transform(Comment $comment)
	{
		return [
            'id' => $comment->id,
			'comment' => $comment->comment,
			'cake_id' => $comment->cake_id,
			'user_id' => $comment->user_id,
			'created_at' => $comment->created_at->toDateTimeString(),
            'created_at_human' => $comment->created_at->diffForHumans()
		];
	}

	/**
     * Will sideload user
     *
     * @return League\Fractal\ItemResource
     */
    public function includeUser(Comment $comment)
    {
        return $this->item($comment->user, new UserTransformer);
    }

    /**
     * Will sidelaod cake
     *
     * @return League\Fractal\ItemResource
     */
    public function includeCake(Comment $comment)
    {
        return $this->item($comment->cake, new CakeTransformer);
    }
}
