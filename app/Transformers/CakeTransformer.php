<?php

namespace App\Transformers;

use App\Cake;
use App\Transformers\UserTransformer;
use App\Transformers\CategoryTransformer;
use App\Transformers\LikeTransformer;
use Illuminate\Support\Facades\Storage;
use League\Fractal\TransformerAbstract;

class CakeTransformer extends TransformerAbstract
{
    /**
     * List of resources to be included
     * 
     * @var array
     */
    protected $availableIncludes = [
        'user','categories','likes'
    ];
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Cake $cake)
    {
        return [
            'id' => $cake->id,
            'name' => $cake->name,
            'description' => $cake->description,
            'picture_url' => $cake->picture_url,
            'thumbnail' => $cake->thumbnail,
            'link' => route('cakes.show', $cake->slug),
            'slug' => $cake->slug,
            'created_at' => $cake->created_at->toDateTimeString(),
            'created_at_human' => $cake->created_at->diffForHumans(),
            'total_likes' => $cake->total_likes,
            'liked_by_current_user' => $cake->liked()
        ];
    }

    /**
     * Will sideload user
     * 
     * @param  Cake   $cake 
     * @return Item
     */
    public function includeUser(Cake $cake)
    {
        return $this->item($cake->user, new UserTransformer);
    }

    /**
     * Will sideload user
     * 
     * @param  Cake   $cake 
     * @return Item
     */
    public function includeCategories(Cake $cake)
    {
        return $this->collection($cake->categories, new CategoryTransformer);
    }

    public function includeLikes(Cake $cake)
    {
        return $this->collection($cake->likes, new LikeTransformer);
    }
}
