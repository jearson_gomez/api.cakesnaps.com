<?php

namespace App\Transformers\Traits;

use Illuminate\Database\Eloquent\Model;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;

trait TransformerTrait
{
	/**
	 * It transforms data
	 * @param  Cake   $cake 
	 * @return Response
	 */
	public function transformItem(Model $model, $transformer, array $relations = [])
	{
		return fractal()
            ->item($model)
            ->parseIncludes($relations)
            ->transformWith($transformer)
            ->toArray();
	}

	public function transformCollection($lists, $transformer, $paginator, array $relations = [])
	{
		return fractal()
            ->collection($lists)
            ->parseIncludes($relations)
            ->transformWith($transformer)
            ->paginateWith(new IlluminatePaginatorAdapter($paginator))
            ->toArray();
	}
	public function transformResponse(Model $model, $transformer, array $relations = [])
	{
		return fractal()
            ->item($model)
            ->parseIncludes($relations)
            ->transformWith($transformer)
            ->respond(201);
	}
}