<?php

namespace App\Transformers;

use App\Like;
use App\Transformers\UserTransformer;
use App\User;
use League\Fractal\TransformerAbstract;

class LikeTransformer extends TransformerAbstract
{
	/**
     * List of resources to be included
     * 
     * @var array
     */
    protected $availableIncludes = [
        'user'
    ];

	public function transform(Like $like)
	{
		return [
			'like_id' => $like->likeable_id,
			'user_id' => $like->user_id
		];
	}

	/**
     * Will sideload user
     * 
     * @param  Cake   $cake 
     * @return Item
     */
    public function includeUser(Like $like)
    {
    	$user = User::find($like->user_id);

        return $this->item($user, new UserTransformer);
    }
}