<?php

namespace App\Transformers;

use App\Cake;
use App\Category;
use App\Transformers\CakeTransformer;
use League\Fractal\TransformerAbstract;

class CategoryTransformer extends TransformerAbstract
{
	/**
     * List of resources to be included
     * 
     * @var array
     */
    protected $availableIncludes = [
        'cakes','categories'
    ];

	/**
     * A Fractal transformer.
     *
     * @return array
     */
	public function transform(Category $category)
	{
		return [
            'id' => $category->id,
			'name' => $category->name,
			'description' => $category->description,
			'slug' => $category->slug,
            'picture_url' => $category->picture_url
		];
	}

	/**
     * Include Cake
     *
     * @return League\Fractal\ItemResource
     */
    public function includeCakes(Category $category)
    {
        return $this->collection($category->cakes, new CakeTransformer);
    }

        /**
     * Include Cake
     *
     * @return League\Fractal\ItemResource
     */
    public function includeCategories(Cake $cake)
    {
        return $this->collection($cake->categories, new CategoryTransformer);
    }
}