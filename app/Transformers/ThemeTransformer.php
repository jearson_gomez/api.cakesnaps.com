<?php

namespace App\Transformers;
use App\Theme;
use App\Transformers\UserTransformer;
use App\Transformers\CakeTransformer;
use League\Fractal\TransformerAbstract;
use Carbon\Carbon;

class ThemeTransformer extends TransformerAbstract
{
	
	
    /**
     * List of resources to be included
     * 
     * @var array
     */
    protected $availableIncludes = [
        'cakes', 'winner', 'user'
    ];
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Theme $theme)
    {
        return [
            'id' => $theme->id,
            'name' => $theme->name,
            'description' => $theme->description,
            'picture_url' => $theme->picture_url,
            'link' => route('themes.show', $theme->slug),
            'slug' => $theme->slug,
            'created_at' => $theme->created_at->toDateTimeString(),
            'created_at_human' => $theme->created_at->diffForHumans(),
            'end_of_competition' => $theme->end_of_competition
        ];
    }

    /**
     * Will sideload cakes
     * 
     * @param  Cake   $cake 
     * @return Item
     */
    public function includeCakes(Theme $theme)
    {
        return $this->collection($theme->cakes, new CakeTransformer);
    }

    public function includeUser(Theme $theme)
    {
        return $this->item($theme->user, new UserTransformer);
    }

    public function includeWinner(Theme $theme)
    {
        $didDue = Carbon::now()->toDateTimeString() >= $theme->end_of_competition;

    	if (count($collection = $theme->cakes) > 0 && $didDue)
    	{

    		$winner = $collection->where('likes', $collection->max('likes'))->first();

        	return $this->item($winner, new CakeTransformer);
    	}

    }


}