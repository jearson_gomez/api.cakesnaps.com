<?php

namespace App\Transformers;

use App\Transformers\CakeTransformer;
use App\Transformers\UserTransformer;
use App\User;
use League\Fractal\TransformerAbstract;

class UserTransformer extends TransformerAbstract
{
	/**
     * List of resources to be included
     * 
     * @var array
     */
    protected $availableIncludes = [
        'cakes'
    ];

	/**
	 * A Fractal Transformer
	 * 
	 * @param  User   $user 
	 * @return array
	 */
	public function transform(User $user)
	{
		return [
			'id' =>  $user->id,
			'name' => $user->name,
			'email' => $user->email,
			'gender' => $user->gender,
			'timezone' => $user->timezone,
			'location' => $user->location,
			'picture_url' => $user->picture_url,
			'thumbnail' => $user->thumbnail ? : $user->getAvatar(),
			'bio' => $user->bio,
			'birthdate' => $user->birthdate
		];
	}

	/**
	 * Will sideload cakes related to user
	 * 
	 * @param  User   $user 
	 * @return Collection
	 */
	public function includeCakes(User $user)
	{
		return $this->collection($user->cakes, new CakeTransformer);
	}
}