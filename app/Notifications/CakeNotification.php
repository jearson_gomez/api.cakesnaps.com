<?php

namespace App\Notifications;

use App\Cake;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class CakeNotification extends Notification
{
    //use Queueable;


    protected $message;

    protected $userName;

    protected $thumbnail;

    protected $cakeName;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($message, $userName, $thumbnail, $cakeName)
    {
     
        $this->message = $message;
        $this->userName = $userName;
        $this->thumbnail = $thumbnail;
        $this->cakeName = $cakeName;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }


    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toDatabase($notifiable)
    {
        

        return [
            'message' => $this->message,
            'user_name' => $this->userName,
            'cake_picture_url' => $this->thumbnail,
            'cake_name' => $this->cakeName
        ];
    }
}
